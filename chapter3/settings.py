DATABASES = {
	'default': {
		#'ENGINE': 'django.db.backends.sqlite3',
		#'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
		'ENGINE': 'django.db.backends.mysql',
		'NAME': 'django',
		'USER': 'root',
		'PASSWORD': 'humingzhe',
		'HOST': '127.0.0.1',
		'PORT': '3306',
	}
	
	INSTALLED_APPS = [
		'django.contrib.admin',
		'django.contrib.auth',
		'django.contrib.contenttypes',
		'django.contrib.sessions',
		'django.contrib.messages',
		'django.contrib.staticfiles',
		'dashboard',
	]
}